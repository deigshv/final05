from py_singleton import singleton


@singleton
class Gateway:
    """
    todoo найти как в nginx перенаправлять запросы на другой сервер
    """
    def info(self):
        # todoo из gateway надо сделать так чтобы можно заходить на все микросервисы
        # в том числе и базы данных которые находятся
        """
        todo по идеи можно сделать так что
        """
        return 'Gateway.info()'

    def __init__(self):
        pass

    def receive_request(self, user_request):
        return user_request

    def jsonify_request(self, request):
        """
        method for tests only
        :param request:
        :return:
        """
        query = {'question': request}
        return query

