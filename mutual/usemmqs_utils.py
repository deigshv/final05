"""
utils for project
"""
import os
import random
import time

from flask import Response, jsonify

import mutual.exceptions.custom_exceptions


# todo не пишутся логи потому что имя файла надо передавать как параметр иначе он пытается писать в файл которого нет
def proceed_on_fail(logger, value, values):
    """
    logging and raising exception
    :param logger: logger
    :param value: value to check
    :param values: valid collection of values
    """
    if value == 'var':  # todoo
        msg = f' >> var << is special valid, pls add to valid value list {values}'
        logger.error(msg)
        logger.info("step, ")
        raise mutual.exceptions.custom_exceptions.VarValueException(msg)
    logger.info("step, ")
    exception = mutual.exceptions.custom_exceptions.EnvsVarException(f"cannot be {value} ( check : "
                                                             f" 1. check  >> ' << or double quote "
                                                             f" 2. if there extra spaces ) === "
                                                             f" it could be only {values}")
    logger.error(f"step, {exception} ")

    raise exception


def get_required_env_var(logger, envvar: str) -> str:
    if envvar not in os.environ:  # сделать env var exception
        message = f"Please set the {envvar} environment variable"  # todoo проверить везде д б такая ошибка
        e = mutual.exceptions.custom_exceptions.EnvsVarException(message)
        logger.error(f"step, {e} ")
        raise mutual.exceptions.custom_exceptions.EnvsVarException(message)

    return os.environ[envvar]


def check_value_validity(curr_value, values, logger):  # todoo удалить все старые такие же чекеры и мигрировать на этот
    if values is None:  # todoo rename values to valid_values or valid_values_list
        raise mutual.exceptions.custom_exceptions.ConfigException(
            "you need provide collection of valid values for a given configuration point ")
    if curr_value not in values:
        point_ = "pls provide valid value for a given configuration point "
        logger.error(point_)
        raise mutual.exceptions.custom_exceptions.ConfigException(point_)
    if curr_value not in values:
        proceed_on_fail(logger=logger, value=curr_value, values=values)
    return curr_value


def check_health(logger, config) -> Response:
    """Check health of the microservice and mounted volume through online logging to file"""
    time_ = time.time()
    # format time
    time__ = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time_))
    random_number = random.randint(0, 1000)
    logger.info(f'step, mount volume and check logging in file {time__} '
                f'random_number {random_number}')

    response = jsonify({"API health check": '/hc',
                        "random_number": random_number,
                        "time": f'{time__}',
                        "USED PORT": config.PORT,
                        "status": "ok"})

    return response
