from pathlib import Path
from mutual import usemmqs_utils as utils
from mutual.cfgs.log_config import logging_setup

logger = logging_setup("mutual_configs.csv")


def top_api_prefix(service_name: str) -> str:
    # todoo
    return service_name


class ConfigModelsClient:

    def __init__(self, logger):
        self.logger = logger
        self.NUM_SEARCH_IN = \
            utils.check_value_validity(curr_value=utils.get_required_env_var(
                self.logger, 'NUM_SEARCH_IN'),
                values={"1", "2", "3", "4"},
                logger=self.logger)

        # logic

        # artifacts managements

        # Logs:
        self.LOG_PATH = Path('mutual/artifacts/logs/')

        # Api:
        self.TOP_PREFIX = top_api_prefix('models_client')

        # Deploy:
        self.HOST = utils.check_value_validity(curr_value=utils.get_required_env_var(logger, 'HOST'),
                                               values={'localhost', '127.0.0.1',
                                                       'host.docker.internal', "0.0.0.0",
                                                       'localpytest'
                                                       }, logger=self.logger)
        self.PORT = utils.check_value_validity(curr_value=utils.get_required_env_var(self.logger, 'PORT'),
                                               values={'7474', '7575'},
                                               logger=self.logger)
        self.APP_DEBUG_MODE = utils.check_value_validity(
            curr_value=utils.get_required_env_var(self.logger, 'FLASK_DEBUG'),
            values={True, False, "1", 'var'},
            logger=self.logger)


class ConfigUSEMModel:

    def __init__(self, logger):
        self.logger = logger

        # artifacts managements / Paths
        self.MODEL_PATH = ""

        # Logs:
        self.LOG_PATH = Path('mutual/artifacts/logs/')

        # Api:
        self.TOP_PREFIX = top_api_prefix('usemm')  # rename API_PREFIX or something # todoo

        self.MODEL = utils.check_value_validity(curr_value=utils.get_required_env_var(logger, 'MODEL'),
                                                values={"USEMv2", "mock_jsons", "USEMv2wget", 'var'},
                                                # todoo mock_jsons изменить на mock_jsons_4q
                                                logger=self.logger)

        self.HOST = utils.check_value_validity(curr_value=utils.get_required_env_var(logger, 'HOST'),
                                               values={'localhost', '127.0.0.1',
                                                       'host.docker.internal', "0.0.0.0",
                                                       'localpytest', 'var'
                                                       }, logger=self.logger)

        self.PORT = utils.check_value_validity(utils.get_required_env_var(logger, 'USEM_PORT'),
                                               values={'7575', '7474', 'var'},
                                               logger=self.logger)
        self.APP_DEBUG_MODE = utils.check_value_validity(
            curr_value=utils.get_required_env_var(self.logger, 'FLASK_DEBUG'),
            values={True, False, "1", 'var'},
            logger=self.logger)


class ConfigQsCluster:

    def __init__(self, logger):
        self.logger = logger

        # artifacts managements / Paths
        self.CLUSTER_ID = self._check_cluster_id_value(utils.get_required_env_var(logger, "CLUSTER_ID"))



        self.DB_PATH = utils.check_value_validity(utils.get_required_env_var(logger, "DB_PATH"),
                                                  values={'artifacts', 'var'},
                                                  logger=self.logger)

        self.IDX_MODEL_PATH = utils.check_value_validity(utils.get_required_env_var(logger, "IDX_MODEL_PATH"),
                                                         values={'artifacts', 'var'},
                                                         logger=self.logger)

        self.BASE_NAME_IDX_MODEL = utils.check_value_validity(curr_value='index_200_cl0',
                                                              values={'index_200_cl0',
                                                                      'index_2000_cl0',
                                                                      'index_20k_cl0',
                                                                      'index_all_cl0', 'var'},
                                                              logger=self.logger)
        self.BASE_NAME_QS_DB = utils.check_value_validity(curr_value='sample200_qs_n_emb_cl0',
                                                          values={'sample200_qs_n_emb_cl0',
                                                                  'sample2000_qs_n_emb_cl0',
                                                                  'sample20k_qs_n_emb_cl0',
                                                                  'sample_all_qs_n_emb_cl0', 'var'},
                                                          logger=self.logger)
        self.EXT_IDX_MODEL = utils.check_value_validity(curr_value='.faiss',
                                                        values={'.faiss', 'var'},
                                                        logger=self.logger)
        self.EXT_QS_DB = utils.check_value_validity(curr_value='.pkl',
                                                    values={'.pkl', 'var', '.csv'},
                                                    logger=self.logger)

        # Logs:
        self.LOG_PATH = Path('mutual/artifacts/logs/')

        # Api:
        self.TOP_PREFIX = top_api_prefix('qsc')

        # Deploy:
        self.HOST = utils.check_value_validity(curr_value=utils.get_required_env_var(logger, 'HOST'),
                                               values={'localhost', '127.0.0.1', 'var',
                                                       'host.docker.internal', "0.0.0.0",
                                                       }, logger=self.logger)

        self.qs_PORT = self._check_port(utils.get_required_env_var(logger, 'PORT'))

        self.APP_DEBUG_MODE = utils.check_value_validity(
            curr_value=utils.get_required_env_var(self.logger, 'FLASK_DEBUG'),
            values={True, False, "1", 'var'},
            logger=self.logger)

    def _check_port(self, port):  # todoo перенести в utils и заменить
        valid_values = {'7580', '7581', '7582', '7583', '7474', '7575', 'var'}
        if port not in valid_values:
            utils.proceed_on_fail(logger=self.logger, value=port, values=valid_values)
        return port

    def _check_cluster_id_value(self, idx):  # todoo перенести в utils и заменить
        """
        этот параметр нужен для REST API , для порта на который будет сетиться для того, чтобы понять
        где искать бд и нужную faiss модель
        :param idx:
        :return:
        """
        valid_values = {'0', '1', '2', '3', '99', 'var'}
        if idx not in valid_values:
            utils.proceed_on_fail(logger=self.logger, value=idx, values=valid_values)
        return idx







# run main here

