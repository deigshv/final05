import logging
import os


def logging_setup(filename, log_detals='medium_level_02', logfile_path='mutual/logs/', log_level=logging.INFO):
    log_name_base = os.path.basename(filename).split(".")[0]
    # log_ext  =  mc_config.LOG_FILE_EXT # todoo
    log_ext = 'csv'
    logger_filename = f'%s.{log_ext}' % (logfile_path + log_name_base)

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(log_level)

    detals_level = "None"
    if log_detals == 'medium_level_01':
        detals_level = '%(asctime)s%(msecs)07d, %(message)s, %(funcName)s, %(lineno)s, ' + \
                       '%(levelname)s, %(filename)s, logger %(name)s, %(threadName)s,' + \
                       '%(thread)s, %(processName)s, %(process)s, %(module)s, '
    elif log_detals == 'medium_level_02':
        detals_level = '%(asctime)s%(msecs)07d,%(message)s,%(funcName)s,%(lineno)s, ' + \
                       '%(filename)s, %(module)s, '

    if detals_level == "None":
        raise ValueError('log_detals is not valid')
    logging.basicConfig(
        level=log_level,
        format=detals_level,
        style='%',
        datefmt='%d,%H,%M.%S',
        handlers=[
            logging.FileHandler(filename=logger_filename, mode='a'),
            stream_handler
        ]
    )
    logger = logging.getLogger(__name__)
    return logger
