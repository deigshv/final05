from abc import (
    ABC,
)
import difflib
from typing import Dict
import numpy as np
import pandas as pd

class QsClusterInterface(ABC):
    """
    Interface for QsCluster.
    """

    def __init__(self, cluster_id, num_qs2fetch: int, data_path: str = None, model_path: str = None, ):

        self.cluster_id = cluster_id  # id of cluster
        self.num_qs = num_qs2fetch

    def get_closest_match(self, query: str):  # db_connection: List[Tuple[str, np.array]]
        """
        method for debugging purpose only
        self.db_connection type is  List[Tuple[str, np.array]]
        :param query:
        :return:
        """
        # find the closest string to query in self.db_connection return string and index in self.db_connection
        db_only_strings = [i[0] for i in self.db_connection]
        # find = difflib.get_close_matches(query, self.db_connection, 1, 0.5)
        find = difflib.get_close_matches(query, db_only_strings, 1, 0.5)
        if len(find) == 0:
            return {'status': 404}
        index = db_only_strings.index(find[0])
        return {'status': 200, 'find': find, 'index': index}

    def _model_search(self, query):

        simularity_distance, qs_indeces = self.model_path.search(np.array([query]), self.num_qs)
        return simularity_distance, qs_indeces

    def _dejsonify(self, key, data):
        # dejson data
        data = data[key]
        return data

    def _jsonify_pandas(self, data, columns):
        # json data
        # add data type to columns in pandas dataframe | int | int | float |
        data = pd.DataFrame(data, columns=columns)
        response = pd.DataFrame(data, columns=columns)
        # convert Pandas DataFrame to json
        response = response.to_json(orient='records')
        return response

    def cluster_search(self, query) -> np.array:  # todo should json
        query = self._dejsonify('embeddings', query)
        D, I = self._model_search(query)
        status = self._fetch_top_qs_from_db(I)  # todo should in separate thread with retry and stuff
        if status != 200:
            # todo retry
            pass
        simularity_distance = np.array(D)
        qs_indices = np.array(I)  # todo do u need this > ? ~~~
        qs_indices_distance = np.concatenate((qs_indices.T, simularity_distance.T), axis=1)
        response = np.concatenate(
            (np.array([self.cluster_id] * self.num_qs).reshape(-1, 1), qs_indices_distance), axis=1)
        response = self._jsonify_pandas(response, columns=['cluster_id', 'index', 'distance'])
        return response

    def get_qs(self, index: str):
        index_ = self.memcache_w_top_qs[index]
        return index_

    def _fetch_top_qs_from_db(self, indices) -> Dict[str, str]:
        # fetch data from database
        self.memcache_w_top_qs = None
        indices_1 = indices[0]
        # get from tuple self.memcache list of questions ( only first value from the tuple) convert to Dict where key is string representation of indices_1
        indices_ = {str(i): self.db_connection[i][0] for i in indices_1}
        # indices_ = dict(zip(indices_1, [self.memcache[i][0] for i in indices_1]))
        if len(indices_) == 0 or len(indices_) != self.num_qs:
            return {'status': '500'}
        self.memcache_w_top_qs = indices_
        return {'status': '200'}
        # return

    def _response_wrapper(self, data):
        """
        wrapper that form response in correct form ( json  and stuff ) for communication between microservices
        also logging and stuff
        :param data:
        :return:
        """
        #  todo json
        response = {}
        # response['iteration'] = self.iteration
        # response['cost'] = self.cost
        # response['centroids'] = self.centroids.tolist() # new centroid coordinates
        # response['clusters'] = self.clusters
        return data






