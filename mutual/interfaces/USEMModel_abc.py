from abc import (
    ABC,
    abstractmethod,
)


class USEMModelInterface(ABC):

    @abstractmethod
    def __init__(self):
        pass

    def encode(self, query):
        pass
