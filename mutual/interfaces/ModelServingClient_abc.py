import logging
from typing import List, Dict

import flask
import requests

import numpy as np
import pandas as pd

from numpy import dot

from mutual.exceptions.custom_exceptions import QsClusterManagerCommunicationException, ModelsClientExceptions
from mutual.interfaces.QsCluster_abc import QsClusterInterface
from mutual.interfaces.USEMModel_abc import USEMModelInterface
from models_client import mc_config

logger = logging.getLogger(__name__)


class ModelServingClientInterface:
    def __init__(self,
                 centroids: str,
                 search_in: int,
                 qsc_map: Dict[str, QsClusterInterface] = None,
                 emb_model: USEMModelInterface = None):
        """
        :param emb_model: координаты модели которая будет делать предсказания
        можно на один model_client положить одну USE модель на другой клиент положить другую USE модель с другими параметрами
        и проводить A/B тесты правильно раскидывая трафик между этими клиентами трансформеров.
        :param centroids: файл где лежит координы с центроидами кластеров
        :param search_in: число центройдов из которых будут прилетать ответы. Чем больше тем точнее но медленнее
        """
        logger.info('step, ,')
        self.model: USEMModelInterface = emb_model
        self.cluster_clients = qsc_map

        self.centroids = pd.read_pickle(centroids)
        self.num_cluster_to_choose = search_in  # number of retrieved elements
        if self.num_cluster_to_choose > 4:
            raise Exception("num_cluster_to_choose must be less than 4")

    def get_embeddings(self, query):
        """
        # post request to model
        """
        logger.info(f'step 0 , query = {query},')
        host = mc_config.usemm_cfg_HOST
        if host == '0.0.0.0':
            logger.info('step,_ condition met')
            host = 'host.docker.internal'
        curr_url = 'http://' + host + ':' + mc_config.usemm_cfg_PORT + \
                   "/" + mc_config.usemm_cfg_PREFIX + '/' + mc_config.usemm_cfg_CALC_EMB_ENDPOINT
        logger.info(f'step 012 curr_url =  {curr_url},')
        json_ready = flask.json.dumps(query)
        logger.info('step 2, ')
        try:  # выдает эксепшен опять ругается на json # 404
            response = requests.post(curr_url, json=json_ready)

        except ModelsClientExceptions as exc:
            msg = 'step, If the error starts with ' \
                  'Exception = (Connection aborted., RemoteDisconnected(Remote end closed ' \
                  'connection without response))' \
                  '... ' \
                  'this error could happen when the models is not downloaded yet. So pls stand by... ' \
                  'and try again after few minutes '
            logger.error(f'Exception = {exc} {msg},')
            logger.info(f'step exc curr_url = {curr_url}, Exception = {exc} {msg},')
            raise ModelsClientExceptions(f'Exception = {exc} {msg},') # from exc

        logger.info(f'step 3, ,')
        embeddings = response.json()
        logger.info('step, ,')
        return embeddings

    def sent_query_to_chosen_cluster(self, query: np.array, cluster: int) -> np.array:
        """
        :param query:
        :param cluster:
        :return:
        """
        if mc_config.APP_DEBUG_MODE:
            logger.info(f'step, bf')
            logger.info(f'step, sending to cluster = {cluster} =>  {str(query)[:250]}...  ')
        based_port = mc_config.qsc_config_BASED_PORT  # '758'
        prefix = mc_config.qsc_config_TOP_PREFIX
        host = mc_config.qsc_config_HOST
        if host == '0.0.0.0':
            logger.info(f'step,_ condition met')
            host = 'host.docker.internal'

        url = f'http://{host}:{based_port}{cluster}/{prefix}/{cluster}/search'
        if mc_config.APP_DEBUG_MODE:
            logger.info(f'step, url = {url} ')
        try:
            response = requests.post(url, json=query)
        except Exception as e:
            messages = []
            logger.error(f'Exception = {e},')
            msg = f'step, this url = {url} failed Exception = {e},'
            logger.info(msg)
            messages.append(msg)
            raise QsClusterManagerCommunicationException(f' {e}, {messages} ')
        json_response = response.json()
        logger.info('step, ,')
        return json_response

    def get_answer_from_cluster(self, request: np.array):
        """
        method for communication with cluster image
        :param request:
        :return:
        """
        logger.info('step, ')
        cluster = int(request[0])
        question_id = str(int(request[1]))
        based_port = mc_config.qsc_config_BASED_PORT  # '758'
        prefix = mc_config.qsc_config_TOP_PREFIX
        host = mc_config.qsc_config_HOST
        if mc_config.qsc_config_HOST == '0.0.0.0':
            logger.info('step,132 condition met')
            host = 'host.docker.internal'
        url = f'http://{host}:{based_port}{cluster}/{prefix}/{cluster}/db_query'
        logger.info(f'step, url = {url} ')
        json_ready = self._jsonify('question_id', question_id)
        response = requests.post(url, json=json_ready)
        json = response.json()
        logger.info('step, ')
        return json

    def process_query(self, raw_request):
        """
        :param raw_request:
        :return:
        """
        logger.info('step, ,')
        prep_request = self._prep_raw_request(raw_request)
        query_embedding = self.get_embeddings(prep_request)
        query_embedding = self._dejsonify('embeddings', query_embedding)
        clusters: List[int] = self._calc_distance(query_embedding)
        most_suited_clusters = self._choose_clusters(clusters)
        list_of_suited_questions: List[np.array] = []  # ||
        for cluster_idx in most_suited_clusters:
            vec = self._jsonify('embeddings', query_embedding)  # rename vec_dict
            qs = self.sent_query_to_chosen_cluster(vec, cluster_idx)
            qs_ = self._dejsonify_pandas(qs, columns=['cluster_id', 'question_id', 'distance'])
            list_of_suited_questions.append(qs_)
        list_of_suited_questions = np.concatenate(list_of_suited_questions, axis=0)
        list_of_suited_questions_sorted = list_of_suited_questions[np.argsort(list_of_suited_questions[:, 2])]
        top1 = list_of_suited_questions_sorted[-1]  # todoo вынести это как переменую в конфиг
        question = self.get_answer_from_cluster(top1)
        logger.info('step, ,')
        return question

    def _prep_raw_request(self, raw_request):
        logger.info('step, ,')
        return raw_request

    def _dot(self, A, B):
        return sum(a * b for a, b in zip(A, B))

    def _cosine_similarity1(self, a, b):
        norm = dot(a, b)
        a___ = (dot(a, a) ** .5)
        b___ = (dot(b, b) ** .5)
        return norm / (a___ * b___)

    def _cosine_similarity2(self, vector, matrix):
        logger.info('step, ,')
        np_sum = np.sum(vector * matrix, axis=1)
        n = np.sum(matrix ** 2, axis=1)
        vector = np.array(vector)
        # convert vector from list to np.array

        ndarray = np.sum(vector ** 2)
        ndarray_ = (np_sum / (np.sqrt(n) * np.sqrt(ndarray)))
        logger.info('step, ,')
        return ndarray_

    def _calc_distance(self, query) -> List[int]:
        logger.info('step, ,')
        second_method = True
        values = self.centroids.values()
        l = list(values)
        y = np.array(l)
        x = query
        if second_method:
            logger.info('step, ,')
            distance1 = self._cosine_similarity2(vector=x, matrix=y)
        distance1_ = 1 - distance1
        index1 = distance1_.argsort()
        flatten1 = index1
        indices1 = flatten1.tolist()
        logger.info('step, ,')
        return indices1

    def _choose_clusters(self, clusters) -> List[int]:
        top_clusters = clusters[:self.num_cluster_to_choose]
        return top_clusters

    def _jsonify(self, key: str, data):
        result = {key: data}
        return result

    def _dejsonify(self, key, data):
        data = data[key]
        return data

    def _dejsonify_pandas(self, json_records, columns):
        logger.info('step, ,')
        # convert json_records to pandas dataframe orientation records
        df = pd.read_json(json_records)
        # convert df to np array
        data = df.to_numpy()
        logger.info('step, ,')
        return data

