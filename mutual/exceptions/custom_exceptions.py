# эксепшены тоже переписать по слоям Logic , Log , Deploy etc
class ConfigException(Exception):
    def __init__(self, message):
        super().__init__(message)

        # logger.error(message)


class EnvsVarException(ConfigException):
    def __init__(self, message):
        super().__init__(message)


class VarValueException(EnvsVarException):
    """
    yet another custom exception for make logging reading easier for me
    """

    def __init__(self, message):
        super().__init__(message)


class ModelsClientExceptions(Exception):
    def __init__(self, message):
        super().__init__(message)


class MsCExceptionsConfigs(ModelsClientExceptions):
    def __init__(self, message):
        super().__init__(message)


class MsCExceptionsLogic(ModelsClientExceptions):
    def __init__(self, message):
        super().__init__(f'MsCExceptionsLogic {message}', )


class QsClusterManagerCommunicationException(MsCExceptionsLogic):
    def __init__(self, message):
        super().__init__(f'Error account during Communication with QsClusterManager msg =  {message}')


class MsCExceptionsControllers(ModelsClientExceptions):
    def __init__(self, message):
        super().__init__(f'MsCExceptionsControllers {message}', )


class USEMModel(Exception):
    def __init__(self, message):
        super().__init__()

class USEMModelExceptionsConfigs(USEMModel):
    def __init__(self, message):
        super().__init__('USEMModelExceptionsConfigs')


class USEMModelExceptionsLogic(USEMModel):
    def __init__(self, message):
        super().__init__('USEMModelExceptionsLogic')


class USEMModelExceptionsControllers(USEMModel):
    def __init__(self, message):
        super().__init__('USEMModelExceptionsControllers')
