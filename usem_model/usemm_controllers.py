from usem_model import usemm_logic
import json
from flask import Flask, jsonify, request
from mutual.cfgs.log_config import logging_setup
from usem_model import usemm_cfg
from mutual.usemmqs_utils import check_health

import json_fix
import numpy

json.fallback_table[numpy.ndarray] = lambda array: array.tolist()

FILENAME = __file__
logger = logging_setup(filename=FILENAME)
logger.info(f'step, all configs = {usemm_cfg.ALL_CFGs}')

app = Flask(__name__)
prefix = usemm_cfg.PREFIX

model = usemm_logic.USEMModel(model=usemm_cfg.MODEL)



@app.route('/', methods=['GET', 'POST'])
def info():
    if request.method == 'GET':
        return jsonify({
            "API endpoints": usemm_cfg.PREFIX + '/<endpoint>',
            "GET /": "info",

            "GET /health_check": 'check health of the microservice',
            "GET /return_answer": 'return answer',
            "GET /configs": 'return configs',
            "POST /debug_question ": 'post test question to get embeddings',
            "USED PORT": usemm_cfg.PORT,
        })
    elif request.method == 'POST':
        # logging.info(request.json)
        context = request.get_json()
        echo = jsonify(context)
        logger.info(f'step, echo {echo} ')
        return echo


@app.route(f'/models_client/usemm/calc_embeddings', methods=['POST'])
def calc_embedding():
    if request.method == 'POST':
        logger.info('step, bf controller calc_embeddings POST ,')
        context_ = request.get_json()
        # load json to convert to Dict
        context = json.loads(context_)
        question_ = context['question']
        embeddings = model.encode(question_)
        json_data = json.dumps(embeddings)
        logger.info('step, ,')
        return json_data


@app.route(f'/hc', methods=['GET'])
def hc_and_log():
    if request.method == 'GET':
        response = check_health(logger, usemm_cfg)
        return response


@app.route(f'/{prefix}/hc', methods=['GET', 'POST'])
def health_check_with_log():
    if request.method == 'GET':
        response = check_health(logger, usemm_cfg)
        return response

    elif request.method == 'POST':
        context = request.get_json()
        echo = jsonify(context)
        logger.info(f'step, echo {echo} ')
        return echo


if __name__ == "__main__":
    logger.info('step, ,')
    app.run(host=usemm_cfg.HOST, port=usemm_cfg.PORT)
