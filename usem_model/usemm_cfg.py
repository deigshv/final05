from mutual.cfgs.cfg import ConfigUSEMModel
from mutual.cfgs.log_config import logging_setup

logger = logging_setup('config_logging')
model_server_cfg = ConfigUSEMModel(logger)

MODEL = model_server_cfg.MODEL
# deploy
PORT = model_server_cfg.PORT
HOST = model_server_cfg.HOST
PREFIX = 'models_client' + '/' + model_server_cfg.TOP_PREFIX  # REST API
CALC_EMB_ENDPOINT = 'calc_embeddings'

ALL_CFGs = {
    # logic
    # deploy
    'PORT': PORT,
    'HOST': HOST,
    # api
    'PREFIX': PREFIX,
    'CALC_EMB_ENDPOINT': CALC_EMB_ENDPOINT,
    # logs
    # artifacts
    'MODEL': MODEL,
    # others
    'NAME': 'STM_SERVER',
}
