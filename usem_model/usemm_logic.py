import json
import logging
from typing import Dict

import numpy as np

from sentence_transformers import SentenceTransformer
from mutual.interfaces.USEMModel_abc import USEMModelInterface

logger = logging.getLogger(__name__)


class DefaultEmbeddings:
    def __init__(self):
        with open('usem_model/artifacts/default_embeddings.json') as f:
            self._default_embeddings = json.load(f)
            self._mock_model = self._get_default_embeddings_dict()

    def get_default_embeddings(self, question: str) -> np.array:
        emb = self._mock_model.get(question, None)
        if emb is None:
            return np.zeros(512)
        else:
            return emb

    def _get_default_embeddings_dict(self) -> Dict[str, np.array]:
        # convert json to Dict[str, np.array] where key "query" and value is "embedding" (np.array)
        mapper: Dict[str, np.array] = {}
        keys = self._default_embeddings.keys()
        for key in keys:
            value = self._default_embeddings[key]
            for item in value:
                query_ = item['query']
                embedding_ = item['embeddings']
                mapper[query_] = np.array(embedding_)

        return mapper

    def encode(self, query) -> np.array:
        embeddings = self.get_default_embeddings(query)
        return embeddings


# @singleton
class USEMModel(USEMModelInterface):
    def __init__(self, model: str):
        logger.info('step, ,')
        if model == 'mock_jsons':
            logger.warning(f'step, the model is not loaded we use default embeddings that are zeros or the same '
                           f'for all questions from tests fast and 1m categories ')
            # read json file with default embeddings
            self.model = self._load_default_embeddings()
        elif model == 'USEMv2':
            path_embeddings_model = 'usem_model/artifacts/distiluse-base-multilingual-cased-v2'
            logger.info(f'step, the model is loaded from {path_embeddings_model}')
            self.model = self._load_model(path_embeddings_model)
        elif model == 'USEMv2wget':
            usem_v2 = 'distiluse-base-multilingual-cased-v2'
            bert_basic = 'bert-base-nli-mean-tokens'  # todoo could be tried for A/B testing on TorchServe
            self.model = SentenceTransformer(usem_v2)
            logger.info('step, the model was loaded lets get test embeddings,')  # todoo удалить это все потом
            result = self.model.encode('this is test sentence')
            # get first 10 elements of the embeddings
            result_ = result[:10]
            logger.info(f'step, model ready to use {result_} ')

    def _load_model(self, model_path: str):
        """
        loads model from the path
        :param model_path:
        :return:
        """
        logger.info('step, bf')
        model = SentenceTransformer(model_path)
        return model

    def _load_default_embeddings(self):
        """
        loads default embeddings from json file
        :return:
        """
        logger.info('step, ')
        # read json from the artifacts
        emb = DefaultEmbeddings()
        logger.info('step, ')
        return emb

    def encode(self, query):
        """
        прилетает запрос в ввиде вопроса. Улетает эмбеддинг в ввиде np.array этого вопроса
        :param query: вопрос ( str )
        :return: embeddings of the question ( np.array 512 dimensions )
        """
        logger.info('step,bf ')
        embeddings: np.array = self.model.encode(query)
        # for debugging purposes we will return embeddings with original query
        embeddings__: Dict = self._jsonify(query, embeddings)

        # json_emb = json.dumps(query_embeddings)
        logger.info('step,ef ')
        return embeddings__

    def _jsonify(self, query, embeddings):
        """
        converts np.array to json
        :param query:
        :param embeddings:
        :return:
        """
        logger.info('step, ,')
        query_embeddings: Dict = {
            'query': query,
            'embeddings': embeddings
        }
        logger.info('step, ,')
        return query_embeddings

    def _dejsonify(self, key, json_emb):
        """
        converts json to np.array
        :param json_emb:
        :return:
        """
        logger.info(f'step,bf {json_emb} ,')
        result = json_emb[key]
        logger.info('step,ef ,')
        return result
