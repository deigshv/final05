import difflib
import logging
import faiss
import pandas as pd
import numpy as np
from typing import Dict
from qs_cluster import qsc_config  # this need for logic tests
from mutual.interfaces.QsCluster_abc import QsClusterInterface

logger = logging.getLogger(__name__)


class QsCluster(QsClusterInterface):

    def __init__(self, cluster_id, num_qs2fetch: int, data_path: str = None, model_path: str = None, ):
        # data should be datatype of Path todo
        logger.info('step, ,')
        if data_path is None or model_path is None:
            raise Exception('path need to be provided')
        self.cluster_id = cluster_id  # id of cluster
        data_folder = data_path
        model_folder = model_path
        # PATH_DB4CLUSTER_00 = 'qs_cluster/artifacts/sample200_qs_n_emb_cl00.pkl'
        # PATH_IDX_MODEL_CL00 = 'qs_cluster/artifacts/index_200_cl00.faiss'
        db_path = f"qs_cluster/{data_folder}/{qsc_config.BASE_NAME_QS_DB}{cluster_id}{qsc_config.EXT_QS_DB}"
        model_path = f"qs_cluster/{model_folder}/{qsc_config.BASE_NAME_IDX_MODEL}{cluster_id}{qsc_config.EXT_IDX_MODEL}"
        self.db_connection = pd.read_pickle(db_path)  # type : List[Tuple[str, np.array]]
        self.model = faiss.read_index(model_path)
        self.memcache_w_top_qs = None  # top similar questions
        logger.info('step, ,')
        self.num_qs = num_qs2fetch

    def get_closest_match(self, query: str):  # db_connection: List[Tuple[str, np.array]]
        """
        method for debugging purpose only
        self.db_connection type is  List[Tuple[str, np.array]]
        :param query:
        :return:
        """
        logger.info('step, ,')
        # find the closest string to query in self.db_connection return string and index in self.db_connection
        db_only_strings = [i[0] for i in self.db_connection]
        # find = difflib.get_close_matches(query, self.db_connection, 1, 0.5)
        find = difflib.get_close_matches(query, db_only_strings, 1, 0.5)
        if len(find) == 0:
            return {'status': 404}
        index = db_only_strings.index(find[0])
        logger.info('step, ,')
        return {'status': 200, 'find': find, 'index': index}

    def _model_search(self, query):
        """
        dtype=np.float32 important for faiss https://github.com/facebookresearch/faiss/issues/461
        :param query:
        :return:
        """
        # each element should be np.array with float32
        logger.info('step, ,')
        faiss_ready = np.array([query], dtype=np.float32)
        simularity_distance, qs_indeces = self.model.search(faiss_ready, self.num_qs)
        logger.info('step, ,')
        return simularity_distance, qs_indeces

    def _dejsonify(self, key, data):
        # dejson data
        data = data[key]
        return data

    def _jsonify_pandas(self, data, columns):
        # add data type to columns in pandas dataframe | int | int | float |
        data = pd.DataFrame(data, columns=columns)
        response = pd.DataFrame(data, columns=columns)
        response = response.to_json(orient='records')
        return response

    def cluster_search(self, query) -> np.array:  # todo should json
        logger.info('step, ,')
        query = self._dejsonify('embeddings', query)
        D, I = self._model_search(query)
        status = self._fetch_top_qs_from_db(I)
        if status != 200:
            pass
        simularity_distance = np.array(D)
        qs_indices = np.array(I)
        qs_indices_distance = np.concatenate((qs_indices.T, simularity_distance.T), axis=1)
        response = np.concatenate(
            (np.array([self.cluster_id] * self.num_qs).reshape(-1, 1), qs_indices_distance), axis=1)
        #  todo json
        response = self._jsonify_pandas(response, columns=['cluster_id', 'index', 'distance'])
        logger.info('step, ,')
        return response

    def get_qs(self, index: str):
        logger.info('step, ,')
        index_ = self.memcache_w_top_qs[index]
        logger.info('step, ,')
        return index_

    def _fetch_top_qs_from_db(self, indices) -> Dict[str, str]:
        # fetch data from database
        logger.info('step, ,')
        self.memcache_w_top_qs = None
        indices_1 = indices[0]
        # get from tuple self.memcache list of questions ( only first value from the tuple) convert to Dict where key is string representation of indices_1
        indices_ = {str(i): self.db_connection[i][0] for i in indices_1}
        # indices_ = dict(zip(indices_1, [self.memcache[i][0] for i in indices_1]))
        if len(indices_) == 0 or len(indices_) != self.num_qs:
            return {'status': '500'}
        self.memcache_w_top_qs = indices_
        return {'status': '200'}

    def _post_new_centroid_coordinates(self):
        """
        метод который будет отправлять в be_client новые координаты центрайда
        :return:
        """
        pass
