from mutual.cfgs.cfg import ConfigQsCluster
from mutual.cfgs.log_config import logging_setup

logger = logging_setup("qs_cluster_config_logger")
qscl = ConfigQsCluster(logger)

# logic # ---

# artifacts # ---

# CLUSTER_ID = qscl.CLUSTER_ID
CLUSTER_ID = qscl.CLUSTER_ID
DB_PATH = qscl.DB_PATH
IDX_MODEL_PATH = qscl.IDX_MODEL_PATH
BASE_NAME_IDX_MODEL = qscl.BASE_NAME_IDX_MODEL
BASE_NAME_QS_DB = qscl.BASE_NAME_QS_DB
EXT_IDX_MODEL = qscl.EXT_IDX_MODEL
EXT_QS_DB = qscl.EXT_QS_DB  # '.pkl'

# api # -- API RELATED --
TOP_PREFIX = 'models_client' + '/' + qscl.TOP_PREFIX
# log

# deploy
HOST = qscl.HOST
logger.info(f"step,host||  just checking = {HOST} ")
PORT = qscl.qs_PORT
BASED_PORT = 758

# ENVIRONMENT

