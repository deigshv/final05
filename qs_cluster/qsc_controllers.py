from qs_cluster import qsc_logic
from qs_cluster import qsc_config
from mutual.cfgs.log_config import logging_setup

import numpy
import json
import json_fix # don't delete this is needed for json_fix convert between list and np.array

json.fallback_table[numpy.ndarray] = lambda array: array.tolist()
from flask import Flask, jsonify, request

app = Flask(__name__)

logger = logging_setup(filename=__file__)

cluster = qsc_logic.QsCluster(cluster_id=qsc_config.CLUSTER_ID,
                              data_path=qsc_config.DB_PATH,
                              model_path=qsc_config.IDX_MODEL_PATH,
                              num_qs2fetch=5)

prefix = qsc_config.TOP_PREFIX


@app.route('/', methods=['GET', 'POST'])
def debug():
    if request.method == 'POST':
        context = request.get_json()
        return f'echo {context}'
    elif request.method == 'GET':
        logger.info('step, ,')
        return jsonify({
            "API endpoints and env vars": qsc_config.TOP_PREFIX + '/<endpoint>',
            "GET /": "info",
            "GET /health_check": 'check health of the microservice',
            "GET /return_answer": 'return answer',
            "GET /configs": 'return configs',
            "POST /debug_question ": 'post test question to get embeddings',
            "USED PORT": qsc_config.PORT,
            "CLUSTER ID": qsc_config.CLUSTER_ID,
            "DB_PATH": qsc_config.DB_PATH,
            "IDX_MODEL_PATH": qsc_config.IDX_MODEL_PATH
        })


@app.route(f'/{prefix}/{qsc_config.CLUSTER_ID}/search', methods=['POST'])
def qsc_search():
    if request.method == 'POST':
        logger.info('step, ,')
        context = request.get_json()
        result = cluster.cluster_search(context)
        json_data = json.dumps(result)
        logger.info('step, ,')
        return json_data


@app.route(f'/{prefix}/{qsc_config.CLUSTER_ID}/db_query', methods=['POST'])
def db_query():
    if request.method == 'POST':
        logger.info('step, ,')
        context = request.get_json()['question_id']
        result = cluster.get_qs(context)
        json_data = json.dumps(result)
        logger.info('step, ,')
        return json_data


if __name__ == "__main__":
    logger.info(f'step, qsc_config.HOST = {qsc_config.HOST} , qsc_config.PORT = {qsc_config.PORT} ')
    app.run(host=qsc_config.HOST, port=qsc_config.PORT)
