from fuzzywuzzy import fuzz

from gateway.gateway import Gateway
from models_client.mc_logic import ModelServingClient
from usem_model.usemm_logic import USEMModel
from qs_cluster.qsc_logic import QsCluster



DB_PATH = 'qs_cluster/artifacts'
IDX_MODEL_PATH = 'qs_cluster/artifacts'

PATH_EMBEDDINGS_MODEL = 'usem_model/artifacts/distiluse-base-multilingual-cased-v2'
PATH_CENTROIDS = 'models_client/artifacts/clusters_centers_use_dg2.pkl'

cluster00 = QsCluster(cluster_id=0, data_path=DB_PATH, model_path=IDX_MODEL_PATH, num_qs2fetch=5)
cluster01 = QsCluster(cluster_id=1, data_path=DB_PATH, model_path=IDX_MODEL_PATH, num_qs2fetch=5)
cluster02 = QsCluster(cluster_id=2, data_path=DB_PATH, model_path=IDX_MODEL_PATH, num_qs2fetch=5)
cluster03 = QsCluster(cluster_id=3, data_path=DB_PATH, model_path=IDX_MODEL_PATH, num_qs2fetch=5)

gateway = Gateway()


usemm01 = USEMModel(model_path=PATH_EMBEDDINGS_MODEL)




test_list = [
    # qs from cluster 00
    'Failures haunt me all the time.How do I cope up?',
    "I am a 5 letter word.  I am normally below u  If u remove my 1st letter   u'll find me above u  "
    "If u remove my 1st & 2nd letters  u cant see me  Answer is really very interesting  "
    "Let us see who solves this.... time limit :- today U can also send to other grps if I?",
    'Is the Haskell Programming From First Principles book worth buying?',
    # qs from cluster 01
    'Failures haunt me all the time.How do I cope up?',
    'How will you interpret my dream?',
    "I'm openly gay and falling in love with my best friend I've known for 5 years he's a really great guy but I'm stuck. What should I do?",
    "My mom won't listen to me when I tell her over and over that I am not transgender. What should I do?",
    "Over the years I have convinced myself that I have  had many illnesses & disorders. It's obvious now that I am a hypochondriac. How can I fix this?",
    # qs from cluster 02
    "'(Cannabis) Which is the best grow guide for an indoor soil setup?'",
    '5 years of business experience* vs studying 5-6 years (master degree) business at college - which will have more knowledge in Business do you think?',
    '"Who are you ?" what could be the best answer for this question in interview?',
    'Any free and good quality movies website (outside US & Canada?',
    'Android Application Development: Which software is used to develop APK files?',
    # qs from cluster 03
    ' what are the examples of  radio frequency identification systems used in tracking ?',
    '"What is the scientific definition of reality?"',
    '"The camera produces blur picture", What will be the aspect in this sentence where the entity is mobile? I m asking in context of Aspect level SA.',
    '"It is very easy to defeat someone, but too hard to win some one". What does the previous sentence mean?',
    '"Each value xi is paired with fi indicating that approximately 100 fi % of data are < xi".Can someone explain this line?',
]

CLUSTER_CLIENT_URLS = {'localhost:port/cluster00': cluster00,
                       'localhost:port/cluster01': cluster01,
                       'localhost:port/cluster02': cluster02,
                       'localhost:port/cluster03': cluster03,
                       }
def test_service_performance_all_cluster():
    client = ModelServingClient(centroids=PATH_CENTROIDS, search_in=4, qsc_map=CLUSTER_CLIENT_URLS, emb_model=usemm01)
    print('overall request number ', len(test_list))

    query_id = -1
    pass_query = 0
    failed_query = 0
    cumulative_sim_score = 0
    for query in test_list:
        query_id += 1
        response_query_question = gateway.jsonify_request(query)
        response = client.process_query(response_query_question)

        # compare how many characters in the string line the same
        stripped = response.strip()
        score = fuzz.token_sort_ratio(stripped, query)
        if score < 80:
            failed_query += 1

        else:
            pass_query += 1
        cumulative_sim_score += score
    print(f'failed queries counts {failed_query}')
    print(f'cum score {cumulative_sim_score}')
    assert failed_query < 0.01 * len(test_list)
    assert cumulative_sim_score == 1800


def test_service_performance_3_cluster():
    client = ModelServingClient(centroids=PATH_CENTROIDS, search_in=3, qsc_map=CLUSTER_CLIENT_URLS, emb_model=usemm01)
    print('overall request number ', len(test_list))
    query_id = -1
    pass_query = 0
    failed_query = 0
    cumulative_sim_score = 0
    for query in test_list:
        query_id += 1
        response_query_question = gateway.jsonify_request(query)
        response = client.process_query(response_query_question)

        stripped = response.strip()
        score = fuzz.token_sort_ratio(stripped, query)
        if score < 80:
            # print('score: ', score
            # print('query: ', query)
            # print('response: ', stripped)
            # print('----------------------')
            failed_query += 1

        else:
            pass_query += 1
        cumulative_sim_score += score
    print('assert failed query number ', failed_query)
    print(f'cum score {cumulative_sim_score}')
    assert failed_query < 0.34 * len(test_list)
    assert cumulative_sim_score == 1430

def test_service_performance_3_cluster_faster_controllers_same():
    """
    the same question like in test_controllers_slow faster
    """

    test_list = [
        # qs from cluster 00
        # 'Failures haunt me all the time.How do I cope up?',
        'Is the Haskell Programming From First Principles book worth buying?',
        # qs from cluster 01
        'Failures haunt me all the time.How do I cope up?',
        # qs from cluster 02
        "'(Cannabis) Which is the best grow guide for an indoor soil setup?'",
        # qs from cluster 03
        ' what are the examples of  radio frequency identification systems used in tracking ?',
    ]

    client = ModelServingClient(centroids=PATH_CENTROIDS, search_in=3, qsc_map=CLUSTER_CLIENT_URLS, emb_model=usemm01)
    print('overall request number ', len(test_list))
    query_id = -1
    pass_query = 0
    failed_query = 0
    cumulative_sim_score = 0
    for query in test_list:
        query_id += 1
        response_query_question = gateway.jsonify_request(query)
        response = client.process_query(response_query_question)

        stripped = response.strip()
        score = fuzz.token_sort_ratio(stripped, query)
        if score < 80:
            print('failed query : ', query)
            print('failed query stripped response : ', stripped)
            print(f' scores of failed query {score}')
            failed_query += 1

        else:
            pass_query += 1
        cumulative_sim_score += score
    print('assert failed query number ', failed_query)
    print(f'cum score {cumulative_sim_score}')
    # assert failed_query < 0.34 * len(test_list)
    assert cumulative_sim_score == 222


def before_all(context):
    pass # todo

# logs
# torch.util distance :
# ======================= 2 passed, 20 warnings in 4.63s ========================
# ======================= 2 passed, 20 warnings in 4.76s ========================
# ======================= 2 passed, 20 warnings in 4.65s ========================
# ======================= 2 passed, 20 warnings in 4.83s ========================
# 1 sec 168
# 1 sec 238
# 1 sec 191
# 1 sec 254
