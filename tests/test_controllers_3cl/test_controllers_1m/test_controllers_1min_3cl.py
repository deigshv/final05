import os

import requests
from fuzzywuzzy import fuzz

test_list = [
    # qs from cluster 00
    " I am a 5 letter word.  I am normally below u  If u remove my 1st letter   u'll find me above u  "
    "If u remove my 1st & 2nd letters  u cant see me  Answer is really very interesting  "
    # qs from cluster 01
    'Failures haunt me all the time.How do I cope up?',
    'How will you interpret my dream?',
    # qs from cluster 02
    "'(Cannabis) Which is the best grow guide for an indoor soil setup?'",
    '5 years of business experience* vs studying 5-6 years (master degree) business at college - which will have more knowledge in Business do you think?',
    # qs from cluster 03
    ' what are the examples of  radio frequency identification systems used in tracking ?',
    '"What is the scientific definition of reality?"',
]

def test_service_performance_3_cluster():  # test_service_performance_3_cluster
    print('overall request number ', len(test_list))
    query_id = -1
    pass_query = 0
    failed_query = 0
    cumulative_sim_score = 0
    for query in test_list:
        query_id += 1
        query_json = {"question": query}
        url = f'http://127.0.0.1:7474/'
        response = requests.post(url, json=query_json)
        response_json = response.json()
        stripped = response_json.strip()
        score = fuzz.token_sort_ratio(stripped, query)
        if score < 80:
            print('failed_query query_id = ', query_id)
            failed_query += 1

        else:
            print('pass_query query_id = ', query_id)
            pass_query += 1
        cumulative_sim_score += score
    print(f'failed queries counts {failed_query}')
    print(f'cum score {cumulative_sim_score}')
    assert cumulative_sim_score == 468
