import os
import requests
from fuzzywuzzy import fuzz

test_list = [
    # qs from cluster 00
    'Is the Haskell Programming From First Principles book worth buying?',
    # qs from cluster 01
    'Failures haunt me all the time.How do I cope up?',
    # qs from cluster 02
    "'(Cannabis) Which is the best grow guide for an indoor soil setup?'",
    # qs from cluster 03
    ' what are the examples of  radio frequency identification systems used in tracking ?',
    # ===================== # mock model failed on those qs below
    # cl00
    'Is the Haskell Programming From First Principles book worth buying?',
    # cl01
    "Over the years I have convinced myself that I have  had many illnesses & disorders. "
    "It's obvious now that I am a hypochondriac. How can I fix this?",
    # cl02
    'Android Application Development: Which software is used to develop APK files?',
    # cl03
    '"Each value xi is paired with fi indicating that approximately 100 fi % of data are '
    '< xi".Can someone explain this line?',
]


def test_service_performance_3_cluster():
    print('overall request number ', len(test_list))

    query_id = -1
    pass_query = 0
    failed_query = 0
    cumulative_sim_score = 0
    failed_query_ids = []
    failed_query_response_scores = []
    for query in test_list:
        query_id += 1
        query = {'question': query}
        host = os.environ["HOST"]
        port = os.environ["PORT"]

        url = f'http://{host}:{port}/'

        response = requests.post(url, json=query)
        response_ = response.json()
        stripped = response_.strip()
        score = fuzz.token_sort_ratio(stripped, query)
        if score < 80:
            failed_query_response_scores.append((query, stripped, score))
            print('failed query : ', query)
            failed_query_ids.append(query_id)
            print('failed query stripped response : ', stripped)
            print(f' scores of failed query {score}')
            failed_query += 1

        else:
            pass_query += 1
        cumulative_sim_score += score
    print(f'failed queries counts {failed_query}')
    print(f'cum score {cumulative_sim_score}')
    print('os.environ["NUM_SEARCH_IN"] = ', os.environ["NUM_SEARCH_IN"])
    print('os.environ["MODEL"] = ', os.environ["MODEL"])
    if os.environ["NUM_SEARCH_IN"] == "3" and os.environ["MODEL"] == 'mock_jsons':
        print('failed_query data Tuple(query, response, score) , ', failed_query_response_scores)
        print('failed_query_id , ', failed_query_ids)
        assert cumulative_sim_score > 196
        assert cumulative_sim_score < 212
        assert failed_query == 3
    if os.environ["NUM_SEARCH_IN"] == "3" and os.environ["MODEL"] != 'mock_jsons':
        print('failed_query data Tuple(query, response, score) , ', failed_query_response_scores)
        print('failed_query_ids , ', failed_query_ids)
        # assert cumulative_sim_score > 196
        assert cumulative_sim_score == 549
        assert failed_query == 4
        assert failed_query_ids == [0, 1, 3, 4]

    elif os.environ["NUM_SEARCH_IN"] == "4" and os.environ["MODEL"] == 'mock_jsons':
        print('failed_query data Tuple(query, response, score) , ', failed_query_response_scores)
        print('failed_query_id , ', failed_query_ids)
        assert cumulative_sim_score == 494
        assert failed_query == 4
        # assert that  failed_query_id is [1,5,6,7]
        assert failed_query_ids == [1, 5, 6, 7]

    elif os.environ["NUM_SEARCH_IN"] == "4" and os.environ["MODEL"] != 'mock_jsons':
        print('failed_query data Tuple(query, response, score) , ', failed_query_response_scores)
        print('failed_query_id , ', failed_query_ids)
        assert cumulative_sim_score == 754
        assert failed_query == 0
        # assert that  failed_query_id is [1,5,6,7]
        assert failed_query_ids == []
