import os

import requests
from fuzzywuzzy import fuzz

test_list = [
    # qs from cluster 00
    " I am a 5 letter word.  I am normally below u  If u remove my 1st letter   u'll find me above u  "
    "If u remove my 1st & 2nd letters  u cant see me  Answer is really very interesting  "
    "Let us see who solves this.... time limit :- today U can also send to other grps if I?",
    'Is the Haskell Programming From First Principles book worth buying?',
    # qs from cluster 01
    'Failures haunt me all the time.How do I cope up?',
    'How will you interpret my dream?',
    "I'm openly gay and falling in love with my best friend I've known for 5 years he's a really great guy but I'm stuck. What should I do?",
    "My mom won't listen to me when I tell her over and over that I am not transgender. What should I do?",
    "Over the years I have convinced myself that I have  had many illnesses & disorders. It's obvious now that I am a hypochondriac. How can I fix this?",
    # qs from cluster 02
    "'(Cannabis) Which is the best grow guide for an indoor soil setup?'",
    '5 years of business experience* vs studying 5-6 years (master degree) business at college - which will have more knowledge in Business do you think?',
    '"Who are you ?" what could be the best answer for this question in interview?',
    'Any free and good quality movies website (outside US & Canada?',
    'Android Application Development: Which software is used to develop APK files?',
    # qs from cluster 03
    ' what are the examples of  radio frequency identification systems used in tracking ?',
    '"What is the scientific definition of reality?"',
    '"The camera produces blur picture", What will be the aspect in this sentence where the entity is mobile? I m asking in context of Aspect level SA.',
    '"It is very easy to defeat someone, but too hard to win some one". What does the previous sentence mean?',
    '"Each value xi is paired with fi indicating that approximately 100 fi % of data are < xi".Can someone explain this line?',
]




def test_all_services():  # test_service_performance_3_cluster
    print('overall request number ', len(test_list))

    query_id = -1
    pass_query = 0
    failed_query = 0
    cumulative_sim_score = 0
    failed_query_ids = []
    failed_query_response_scores = []
    for query in test_list:
        query_id += 1
        query = {'question': query}  # todoo convert to json explicitly
        host = os.environ["HOST"]
        port = os.environ["PORT"]

        # url = f'http://{host}:7474/'
        url = f'http://{host}:{port}/'

        response = requests.post(url, json=query)
        response_ = response.json()
        stripped = response_.strip()
        score = fuzz.token_sort_ratio(stripped, query)
        if score < 80:
            failed_query_response_scores.append((query, stripped, score))
            print('failed query : ', query)
            failed_query_ids.append(query_id)
            print('failed query stripped response : ', stripped)
            print(f' scores of failed query {score}')
            failed_query += 1

        else:
            pass_query += 1
        cumulative_sim_score += score
    print(f'failed queries counts {failed_query}')
    print(f'cum score {cumulative_sim_score}')
    # assert cumulative_sim_score == 211
    print('os.environ["NUM_SEARCH_IN"] = ', os.environ["NUM_SEARCH_IN"])
    print('os.environ["MODEL"] = ', os.environ["MODEL"])

    if os.environ["NUM_SEARCH_IN"] == "3" and os.environ["MODEL"] != 'mock_jsons':
        print('failed_query data Tuple(query, response, score) , ', failed_query_response_scores)
        print('failed_query_ids , ', failed_query_ids)
        # assert cumulative_sim_score > 196
        assert cumulative_sim_score == 1323
        assert failed_query == 5
        assert failed_query_ids == [1, 2, 4, 5, 12]


    elif os.environ["NUM_SEARCH_IN"] == "4" and os.environ["MODEL"] != 'mock_jsons':
        print('failed_query data Tuple(query, response, score) , ', failed_query_response_scores)
        print('failed_query_id , ', failed_query_ids)
        assert cumulative_sim_score == 1606
        assert failed_query == 0
        assert failed_query_ids == []

    elif os.environ["MODEL"] == 'mock_jsons':
        raise Exception('mock_jsons model is not supported for this test. Check your env vars ')


