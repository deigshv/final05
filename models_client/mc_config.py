from mutual.cfgs.cfg import ConfigModelsClient, ConfigUSEMModel, ConfigQsCluster
from mutual.cfgs.log_config import logging_setup

logger = logging_setup("_mc_config_")

mc = ConfigModelsClient(logger)
logger = logging_setup('config_logging')
model_server_cfg = ConfigUSEMModel(logger)
qsc_config = ConfigQsCluster(logger)

# logic
SEARCH_IN = int(mc.NUM_SEARCH_IN)

# log
LOG_FILE_PATH = mc.LOG_PATH  #
# artifacts

# api
PREFIX = mc.TOP_PREFIX  # 'models_client'
# deploy
PORT = mc.PORT  # '7474'
HOST = mc.HOST  # os.enviro=HOST '0.0.0.0'
APP_DEBUG_MODE = mc.APP_DEBUG_MODE  # True

MC_ALL = {
    # logic
    'SEARCH_IN': SEARCH_IN,
    # deploy
    'PORT': PORT,
    'HOST': HOST,
    'APP_DEBUG_MODE': APP_DEBUG_MODE,
    # api
    'PREFIX': PREFIX,
    # logs
    'LOG_FILE_PATH': LOG_FILE_PATH,
    # others
    'NAME': 'BE',

}

# ==== USEM config ====

usemm_cfg_HOST = model_server_cfg.HOST
usemm_cfg_PORT = model_server_cfg.PORT
usemm_cfg_CALC_EMB_ENDPOINT = 'calc_embeddings'
usemm_cfg_PREFIX = PREFIX + '/' + model_server_cfg.TOP_PREFIX

STM_SERVER_ALL = {
    'NAME': 'STM_SERVER',
    'HOST': usemm_cfg_HOST,
    'PORT': usemm_cfg_PORT,
    'CALC_EMB_ENDPOINT': usemm_cfg_CALC_EMB_ENDPOINT,
    'PREFIX': usemm_cfg_PREFIX,
}

qsc_config_BASED_PORT = '758'
qsc_config_TOP_PREFIX = PREFIX + '/' + qsc_config.TOP_PREFIX
qsc_config_HOST = qsc_config.HOST

QSC_MANAGER_ALL = {
    'NAME': 'QSC_MANAGER',
    'HOST': qsc_config_HOST,
    'BASE PORT': qsc_config_BASED_PORT,
    'PREFIX': qsc_config_TOP_PREFIX,
}
