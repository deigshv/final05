from typing import Dict

import numpy as np
import pandas as pd

from mutual.interfaces.USEMModel_abc import USEMModelInterface
from mutual.interfaces.QsCluster_abc import QsClusterInterface
from mutual.interfaces.ModelServingClient_abc import ModelServingClientInterface


class ModelServingClient(ModelServingClientInterface):  # todo перепименовать в USEMMClient
    def __init__(self,
                 centroids: str,
                 search_in: int,
                 qsc_map: Dict[str, QsClusterInterface],
                 emb_model: USEMModelInterface):
        """
        :param emb_model: координаты модели которая будет делать предсказания
        можно на один model_client положить одну USE модель на другой клиент положить другую
        USE модель с другими параметрами
        и проводить A/B тесты правильно ( Это в случае если модель сервится не на TorchServe )
        раскидывая трафик между этими клиентами трансформеров.
        :param centroids: файл где лежит координы с центроидами кластеров
        :param num_centroids: число центройдов из которых будут прилетать ответы. Чем больше
        тем точнее но медленнее
        :param search_in : кол-во кластеров которых будет проходить поиск
        """
        # super().__init__()  #
        self.model: USEMModelInterface = emb_model
        self.cluster_clients = qsc_map
        self.centroids = pd.read_pickle(centroids)
        self.num_cluster_to_choose = search_in
        if self.num_cluster_to_choose > 4:
            raise Exception("num_cluster_to_choose must be less than 4")

    def sent_query_to_chosen_cluster(self, vec, cluster: int) -> np.array:
        url = f'localhost:port/cluster0{cluster}'
        cluster_client: QsClusterInterface = self.cluster_clients[url]
        response_to_models_client = cluster_client.cluster_search(vec)
        return response_to_models_client

    def get_embeddings(self, query):
        embeddings = self.model.encode(query)
        return embeddings

    def get_answer_from_cluster(self, request: np.array):
        """
        method for communication with cluster image
        :param request:
        :return:
        """
        #  todo json
        cluster_id = int(request[0])
        question_id = str(int(request[1]))
        url = f'localhost:port/cluster0{cluster_id}'
        cluster: QsClusterInterface = self.cluster_clients[url]
        question = cluster.get_qs(question_id)
        return question
