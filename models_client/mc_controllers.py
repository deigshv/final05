import logging

from models_client import mc_config
from flask import Flask, jsonify, request
from mutual.interfaces.ModelServingClient_abc import ModelServingClientInterface
from mutual.cfgs.log_config import logging_setup
from mutual.usemmqs_utils import check_health

logger = logging_setup(filename=__file__, log_level=logging.INFO)
env_var = mc_config.MC_ALL
logger.info("step, -- ENV VARS -- {}".format(env_var))

app = Flask(__name__)
if not app.config["DEBUG"]:
    assert app.config["DEBUG"] == mc_config.APP_DEBUG_MODE
    logger.warning("\n step, -- NOT A DEBUG MODE -- \n ")

prefix = mc_config.PREFIX
port = mc_config.PORT
client = ModelServingClientInterface(centroids='models_client/artifacts/'
                                               'clusters_centers_use_dg2.pkl',
                                     search_in=mc_config.SEARCH_IN)


@app.route('/', methods=['GET', 'POST'])  # --port=7474
def debug():
    if request.method == 'POST':
        logger.info('step, ,')
        context = request.get_json()
        modified_context_ = context['question']
        modified_context = modified_context_.replace(',', '')
        logger.info(f'step, new request with context = {modified_context} ')
        context = client.process_query(context)
        result = jsonify(context)
        return result
    elif request.method == 'GET':
        logger.info('step, GET ')
        return jsonify({
            "API endpoints": prefix + '/<endpoint>',
            "GET /": "info",
            "GET /health_check": 'check health of the microservice',
            "GET /return_answer": 'return answer',
            "GET /configs": 'return configs ( line 50 ) ',
            "POST /debug_question ": 'post test question to get embeddings',
            "USED PORT": mc_config.PORT,
        })


@app.route(f'/hc', methods=['GET'])
def hc_and_log():
    logger.info('step, 86 mc_controllers GET hc')
    if request.method == 'GET':
        response = check_health(logger, mc_config)
        return response


@app.route(f'/{prefix}/hc', methods=['GET', 'POST'])
def health_check_with_log():
    if request.method == 'GET':
        response = check_health(logger, mc_config)
        return response

    elif request.method == 'POST':
        # logging.info(request.json)
        context = request.get_json()
        echo = jsonify(context)
        logger.info(f'step, echo {echo},')
        return echo


if __name__ == "__main__":
    app.run(host=mc_config.HOST, port=mc_config.PORT)
